"use strict";
const ctxt_pos = 0,
    ctxt_title = 1,
    ctxt_modal = 2,
    ctxt_prodlist = 3;

var sto = window.__sto,
    settings = require("./../../settings.json"),
    $ = window.jQuery,
    html = require("./main.html"),
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    style = require("./main.css"),
    format = settings.format,
    custom = settings.custom,
    products = [],
    creaid = settings.creaid,
    btfpos = window.__sto.tracker().value("rp").indexOf('recherche') > -1 ? 'top' : 'mid',
    helper_methods = sto.utils.retailerMethod,
    url_crawling = settings.crawl,
    sto_global_nbproducts = 0,
    sto_global_products = [],
    sto_global_modal,
    promise,
    pdf,
    btnSize = settings.btnSize == "" ? "small" : settings.btnSize,
    prodsPerRow,
    totalPrice = 0,
    prodsWidth,
    allVisibleProds,
    options = {},
    redundantProds = {},
    buttonsH, butterflyH, ctaH, marginTop,
    abkCreateFormat,
    nbSiblings,
    oldNbSiblings = 0,
    toggleInstall = "off",
    toggleAllABK = true,
    inventory = true,
    firstStart = true,
    isThereACPL,
    finalPos;

var i = 0,
    myFormat = -1,
    otherFormat = 99,
    allFormats = __sto.getTrackers();
for (var i = 0; i < allFormats.length; i++) {
    //myFormat = allFormats[i]["type"] == "CPL" && allFormats[i]["crea_id"] == settings.creaid ? i : -1;
    //otherFormat = allFormats[i]["type"] == "CPL" && allFormats[i]["crea_id"] != settings.creaid ? i : -1;

    if (allFormats[i]["type"] == "CPL") {

        if (allFormats[i]["crea_id"] == settings.creaid) {
            myFormat = i;
        } else {
            otherFormat = i;
        }

    }
}
var recuPos = myFormat < otherFormat ? 1 : 2;

try {
    //Google Font Roboto
    var fontRoboto = document.createElement('link');
    fontRoboto.setAttribute('rel', 'stylesheet');
    fontRoboto.setAttribute('href', 'https://fonts.googleapis.com/css?family=Roboto:100,400');
    var head = document.querySelector('head');
    head.appendChild(fontRoboto);


    custom.sort(function(a, b) {
        return a[ctxt_pos] > b[ctxt_pos]
    });

    // Get all product ids from custom product organization
    custom.forEach(function(e) {
        e[ctxt_prodlist].forEach(function(p) {
            products.push(p);
        })
    });

    module.exports = {
        init: _init_()
    }

    function _init_() {
        promise = sto.utils.deferred().resolve();
        sto.load([format, settings.creaid], function(tracker) {

            var removers = {};
            removers[format] = function() {
                style.unuse();
                container.remove();
            };

            if (__sto.tracker().value("ci") !== null) {

                prodsRow();

                isThereACPL = recuPos == 2 ? true : false;

                switch (prodsPerRow) {
                    case 5:
                    case 4:
                        finalPos = isThereACPL ? (prodsPerRow * 5) : (prodsPerRow * 2);
                        break;
                    case 3:
                        finalPos = isThereACPL ? (prodsPerRow * 7) : (prodsPerRow * 3);
                        break;
                    case 2:
                        finalPos = isThereACPL ? (prodsPerRow * 9) : (prodsPerRow * 5);
                        break;
                    case 1:
                        if (document.getElementById('BannerHighCoPLP1') && document.getElementById('BannerHighCoPLP1').style.display == 'none') {
                            finalPos = isThereACPL ? ((prodsPerRow * 18) + 1) : ((prodsPerRow * 10) + 1);
                        } else {
                            finalPos = isThereACPL ? (prodsPerRow * 18) : ((prodsPerRow * 10));
                        }
                        break;
                }

                if (allVisibleProds.length < finalPos) {
                    tracker.error({
                        "tl": isThereACPL ? "notEnoughProductsInPageFor2ndPlacement" : "notEnoughProductsInPage"
                    });
                    return false;
                }

                var formatHTML = document.createElement('div');
                formatHTML.innerHTML = html;
                var container = formatHTML.querySelector('#sto-format');

                //isThereACPL = document.querySelector('.sto-bbs') || document.querySelector('.sto-btf') ? true : false;
                container.className += " pos" + recuPos;

                promise.then(function() {
                    helper_methods.crawl(settings.crawl).promise.then(function(d) {
                        try {
                            tracker.context._value.tz = "vignette";

                            var avail = checkAvailability(d, settings.custom),
                                ret = avail[1],
                                relevantProducts = avail[0][0];

                            if (typeof ret === "undefined") {
                                abkCreateFormat = helper_methods.createFormat(container, tracker, relevantProducts, d, 100, null, null);
                            } else {
                                if (ret === false) {
                                    abkCreateFormat = helper_methods.createFormat(container, tracker, relevantProducts, d, 100, null, null);
                                } else if (ret === true) {
                                    abkCreateFormat = helper_methods.createFormat(container, tracker, relevantProducts, d, 100, null, true);
                                    tracker.error({
                                        "tl": "mandatoryNotFound"
                                    });
                                    return removers;
                                }
                            }

                            style.use();
                            container.style.display = 'none';
                            var emplacement = document.querySelectorAll('.products-grid>article')[0];

                            //Append format
                            var emplacementParent = emplacement.parentNode;
                            emplacementParent.insertBefore(container, emplacement);
                            tracker.display();

                            installFormat();
                            relevantProducts.forEach(function(thisProd, i) {
                                //toModalBox(relevantProducts[i])
                                if (redundantProds[thisProd]) {
                                    var numProds = redundantProds[thisProd]["nb"];
                                    redundantProds[thisProd] = {
                                        "nb": (numProds + 1)
                                    }
                                } else {
                                    redundantProds[thisProd] = {
                                        "nb": 1
                                    }
                                }

                                var prodToModalBox = document.querySelector('.sto-' + placeholder + '-bundleboost .sto-product-container>*[data-id="' + thisProd + '"]');
                                document.querySelector('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-products').appendChild(prodToModalBox);
                            });

                            Object.keys(redundantProds).forEach(function(v, i) {
                                var qtyProds = redundantProds[v]['nb'];

                                if (qtyProds > 1) {
                                    var targetedProducts = document.querySelectorAll('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-products>*[data-id="' + v + '"]')
                                    var repeated = document.createElement("div");
                                    repeated.classList.add("sto-product-repeated");
                                    repeated.innerText = 'x' + qtyProds.toString();
                                    targetedProducts[0].appendChild(repeated);

                                    for (var i = 1; i < targetedProducts.length; i++) {
                                        targetedProducts[i].classList.add("sto-hidden");
                                    }
                                }
                            });

                            var visibleProds = document.querySelectorAll('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-products>*:not(.sto-hidden)');
                            visibleProds[visibleProds.length - 1].classList.add("sto-last-visible");

                            firstDisplay();

                            //SET ALL CLICKS
                            //open modal box
                            document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main').addEventListener('click', function(e) {
                                e.preventDefault();
                                tracker.open({
                                    "tl": "clickOnTile"
                                });
                                showNHide(tracker, 'on');
                                tracker.context._value.tz = "modalBox";
                            });

                            //close modal box
                            document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-modalbox .sto-' + placeholder + '-modalbox-close').addEventListener('click', function(e) {
                                e.preventDefault();
                                tracker.close({
                                    "tl": "btnClose"
                                })
                                showNHide(tracker, 'off');
                                tracker.context._value.tz = "vignette";
                            });
                            document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-w-modalbox').addEventListener('click', function(e) {
                                e.preventDefault();
                                tracker.close({
                                    "tl": "outSideBox"
                                })
                                showNHide(tracker, 'off');
                                tracker.context._value.tz = "vignette";
                            });

                            //abk clicks
                            document.querySelector('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-cta>a').addEventListener('click', function(e) {
                                e.preventDefault();
                                addLot(tracker, d);
                            });
                            document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__aside button.addToCart').addEventListener('click', function(e) {
                                e.preventDefault();
                                addLot(tracker, d);
                            });


                            //RESIZE
                            setInterval(function() {
                                //Get the number of prods per row
                                prodsRow();

                                var element = document.querySelector('.sto-' + placeholder + '-bundleboost');
                                var prodGrid = Array.prototype.slice.call(document.querySelectorAll('.products-grid>.product-item, .products-grid>.sto-bbs, .products-grid>.sto-btf, .products-grid>.sto-dummy, .products-grid>.product-item__highco')),
                                    all = prodGrid.filter((el) => el.style.display != 'none'),
                                    //all = document.querySelectorAll('.products-grid>.product-item'),

                                    pos = Array.prototype.slice.call(all).indexOf(element),
                                    finalPos;

                                //if (btfpos == "top" || btfpos == "mid") { // recherche
                                switch (prodsPerRow) {
                                    case 5:
                                    case 4:
                                        finalPos = isThereACPL ? (prodsPerRow * 5) : (prodsPerRow * 2);
                                        break;
                                    case 3:
                                        finalPos = isThereACPL ? (prodsPerRow * 7) : (prodsPerRow * 3);
                                        break;
                                    case 2:
                                        finalPos = isThereACPL ? (prodsPerRow * 9) : (prodsPerRow * 5);
                                        break;
                                    case 1:
                                        if (document.getElementById('BannerHighCoPLP1') && document.getElementById('BannerHighCoPLP1').style.display == 'none') {
                                            finalPos = isThereACPL ? ((prodsPerRow * 18) + 1) : ((prodsPerRow * 10) + 1);
                                        } else {
                                            finalPos = isThereACPL ? (prodsPerRow * 18) : ((prodsPerRow * 10));
                                        }
                                        break;
                                }

                                if ((all.length - 1) / prodsPerRow >= 1 && (all.length - 1) >= (prodsPerRow * 2) - 1) {
                                    if (pos !== finalPos && element) {
                                        element.parentNode.insertBefore(element, all[finalPos + (pos < finalPos ? 1 : 0)]);
                                        element.style.display = 'inline-block';
                                    }
                                } else {
                                    element.style.display = 'none';
                                }
                                //}

                                inventoryCheck()

                            }, 10)
                        } catch (e) {
                            style.unuse();
                            $(".sto-" + placeholder + "-container").remove();
                            tracker.error({
                                "te": "onBuild-format",
                                "tl": e
                            });
                        }
                    });

                });

            } else {
                tracker.error({
                    "tl": "not connected"
                });
            }
            return removers;
        });

        function addLot(tracker, d) {
            if (toggleAllABK == true) {
                abkCreateFormat.addAllProducts(options);
            }
        }

        function showNHide(tracker, state) {

            switch (state) {
                case 'on':
                    document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-w-modalbox').style.display = "block";
                    document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-modalbox').style.display = "block";
                    setTimeout(function() {
                        document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-w-modalbox').setAttribute('data-state', 'on');
                        document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-modalbox').setAttribute('data-state', 'on');

                        setTimeout(function() {
                            if (firstStart == true) {
                                var prodW = document.querySelector('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-products>*').offsetWidth + 1;
                            } else {
                                var prodW = document.querySelector('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-products>*').offsetWidth;
                            }
                            firstStart = false;

                            document.querySelector('.sto-' + placeholder + '-modalbox-products').style.width = (prodW * document.querySelectorAll('.sto-' + placeholder + '-modalbox-products>*:not(.sto-hidden)').length) + 'px';
                        }, 100);

                    }, 10);
                    break;

                case 'off':
                    setTimeout(function() {
                        document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-w-modalbox').style.display = "none";
                        document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-modalbox').style.display = "none";
                    }, 200);
                    document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-w-modalbox').setAttribute('data-state', 'off');
                    document.querySelector('.sto-' + placeholder + '-bundleboost>.sto-' + placeholder + '-modalbox').setAttribute('data-state', 'off');
                    break;
            }
        }

        function prodsRow() {
            var prodsGridWidth = document.querySelector('.products-grid').offsetWidth;
            var prodGrid = Array.prototype.slice.call(document.querySelectorAll('.products-grid>.product-item, .products-grid>.sto-btf, .products-grid>.product-item__highco'));
            allVisibleProds = prodGrid.filter((el) => el.style.display != 'none');
            //allVisibleProds = document.querySelectorAll('.products-grid>article[data-area="product_item"]');

            prodsWidth = document.querySelectorAll('.products-grid>article[data-area="product_item"]')[0].offsetWidth;
            prodsPerRow = Math.floor(prodsGridWidth / prodsWidth) < 1 || Math.floor(prodsGridWidth / prodsWidth) == Infinity ? 1 : Math.floor(prodsGridWidth / prodsWidth);
        }

        /*function toModalBox(cug) {
          var prodToModalBox = document.querySelector('.sto-' + placeholder + '-bundleboost .sto-product-container>*[data-id="' + cug + '"]');
          document.querySelector('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-products').appendChild(prodToModalBox);
        }*/


        function installFormat() {
            document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main').removeAttribute('href');
            document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__header span').innerHTML = settings.tetiere_text;
            document.querySelector('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox .sto-' + placeholder + '-modalbox-header span').innerHTML = settings.tetiere_text;
            document.querySelector('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-cta span').innerHTML = settings.abk_txt;

            if (settings.promo_text != "") {
                document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main .animcom.sto').innerHTML = '<i></i>  ' + settings.promo_text;
            } else {
                document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main .animcom.sto').remove();
            }
        }

        function firstDisplay(tracker) {
            var allProds = document.querySelectorAll('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-products>*');
            if (settings.tile_main_image == "") {
                document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main').setAttribute('data-img', 'multi');

                allProds.forEach(function(prodInList, i) {
                    var currentImgSrc = prodInList.querySelector('picture img').getAttribute('src');
                    document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main picture').innerHTML += '<img src="' + currentImgSrc + '"/>';
                })

            } else {
                document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main').setAttribute('data-img', 'custom');
            }


            //Set main tile title (custom or concatanation)
            if (settings.lable_text == "") {
                document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main .product-item__title span').innerHTML = '';

                allProds.forEach(function(prodInList, i) {
                    var currentTitle = prodInList.querySelector('.product-item__title').innerText;
                    document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main .product-item__title span').innerText += i == (allProds.length - 1) ? currentTitle.trim() : currentTitle.trim() + ' + ';
                });
            } else {
                document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main .product-item__title span').innerHTML = settings.lable_text.trim();
            }

            setNCheckLengthText();

            var productsIds = [];
            allProds.forEach(function(prodInList, i) {
                var currentPrice = parseFloat(prodInList.querySelector('.price-standard__decimal').innerHTML.replace(',', '.') + prodInList.querySelector('.price-standard__cents').innerHTML.replace(',', '.')),
                    currentId = prodInList.getAttribute('data-id');

                totalPrice += currentPrice;
                productsIds.push(currentId);

                if (document.querySelectorAll('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-products>*[data-id="' + currentId + '"]').length > 1) {
                    var optQTY = document.querySelectorAll('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-products>*[data-id="' + currentId + '"]').length;

                    options[currentId] = {
                        "quantity": optQTY
                    };
                }
            });

            document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__aside .price-standard').innerHTML = '<span class="price-standard__decimal">' + totalPrice.toFixed(2).split('.')[0] + '</span><span class="price-standard__cents"> ,' + totalPrice.toFixed(2).toString().split('.')[1] + '</span><span class="price-standard__currency"> &#8364;</span>';

            //update products in relation with cart
            if (window.D && window.D.cart && window.D.cart.cart && window.D.cart.cart.items && D.cartInstance && D.cartInstance._updateProductQuantitySelector) {
                var in_basket = window.D.cart.cart.items.filter(function(item) {
                    return productsIds.indexOf(item["cug"].toString()) > -1
                });
                if (in_basket) {
                    for (var i = 0; i < in_basket.length; i++) {
                        D.cartInstance._updateProductQuantitySelector(in_basket[i]["rideQuantity"], in_basket[i]["cug"]);
                    }
                }
            }
        }

        function setNUnsetAdd(state) {
            switch (state) {
                case 'on':
                    if (toggleAllABK == true) {
                        document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__aside').classList.remove('bbs-inactive');
                        document.querySelector('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-cta').classList.remove('bbs-inactive');
                    }
                    break;
                case 'off':
                    document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__aside').classList.add('bbs-inactive');
                    document.querySelector('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox-cta').classList.add('bbs-inactive');
                    break;
            }
        }

        function inventoryCheck() {
            inventory = true
            document.querySelectorAll(".sto-" + placeholder + "-modalbox-products > .sto-item").forEach(function(v) {
                var qty = parseInt(v.querySelector(".cart-updater-form").getAttribute("data-quantity") != null ? v.querySelector(".cart-updater-form").getAttribute("data-quantity") : 0);
                var mQty = parseInt(v.querySelector(".cart-updater-form").getAttribute("data-stock"));
                if (mQty <= qty) {
                    inventory = false
                }
            });
            setNUnsetAdd(inventory == false ? "off" : "on");
        }

        function setNCheckLengthText() {
            var el = document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main .product-item__title > span').innerText;

            //Desktop description
            document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main .product-item__title > span').innerText = el.length >= 200 ? el.substring(0, 197) + "...".trim() : el.trim();

            //Mobile description
            var mobileDesc = document.createElement("dt");
            mobileDesc.setAttribute('class', 'sto-' + placeholder + '-mobile-desc');
            mobileDesc.innerText = el.length >= 80 ? el.substring(0, 77) + "...".trim() : el.trim();

            var parentDiv = document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main .product-item__title').parentNode;
            var tgt = document.querySelector('.sto-' + placeholder + '-bundleboost>.product-item__main .product-item__title')
            parentDiv.insertBefore(mobileDesc, tgt);
        }

        function checkAvailability(crawledProds, products) {
            var realProducts = [],
                temporary = [
                    [],
                    [],
                    []
                ],
                rowModal = 0,
                toggleModal = true,
                modalMode = false;

            products.forEach(function(thisProductLine, i) {
                var toggleRowProduct = true;
                thisProductLine[3].forEach(function(thisProduct, j) {
                    if (toggleRowProduct === true && Object.keys(crawledProds).includes(thisProduct)) {
                        rowModal++;
                        toggleRowProduct = false;
                        temporary[0].push(thisProduct);
                        temporary[1].push(thisProductLine[1]);
                        temporary[2].push(thisProductLine[0]);
                    }
                });
                if (thisProductLine[2] == true) {
                    modalMode = true;
                    if (rowModal <= 0) {
                        toggleModal = false;
                    }
                }
                rowModal = 0;
            });
            realProducts[0] = temporary[0];
            realProducts[1] = temporary[1];
            realProducts[2] = temporary[2];
            return (modalMode == true ? toggleModal == false ? [realProducts, true] : [realProducts] : [realProducts])
        }
    }
} catch (e) {
    //console.log(e);
}
