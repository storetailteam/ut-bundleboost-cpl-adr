"use strict";

const ctxt_pos = 0,
  ctxt_title = 1,
  ctxt_modal = 2,
  ctxt_prodlist = 3;

var sto = window.__sto,
  settings = require("./../../settings.json"),
  $ = window.jQuery,
  html = require("./main.html"),
  placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
  style = require("./main.css"),
  container = $(html),
  format = settings.format,
  custom = settings.custom,
  products = [],
  btfpos = settings.btfpos,
  container = $(html),
  helper_methods = sto.utils.retailerMethod,
  url_crawling = settings.crawl,
  sto_global_nbproducts = 0,
  sto_global_products = [],
  sto_global_modal,
  promise,
  bundleboost_add = true,
  options = {},
  pdf;

var fontRoboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
$('head').first().append(fontRoboto);


var position = {
  "grid": {
    "top": 1,
    "mid": 5
  },
  "lines": {
    "top": {
      "line": 1,
      "index": 1
    },
    "mid": {
      "line": 2,
      "index": 1
    }
  }
}

custom.sort(function(a, b) {
  return a[ctxt_pos] > b[ctxt_pos]
});
// Get all product ids from custom product organization
custom.forEach(function(e) {
  e[ctxt_prodlist].forEach(function(p) {
    products.push(p);
  })
});



module.exports = {
  init: _init_(),
  styleGutter: style
}

function _init_() {

  if (format === "IM+SK" || format === "SK+SR") {
    promise = sto.globals.highCo.promise;
  } else {
    promise = sto.utils.deferred().resolve();
  }
  sto.load(format, function(tracker) {
    var removers = {};
    removers[format] = function() {
      style.unuse();
      container.remove();
    };
    promise.then(function() {
      helper_methods.crawl(url_crawling, true).promise.then(function(d) {

        var modalCounter = 0,
          currentModal,
          currentProdsList,
          z,
          y,
          count = custom.length;

        for (z = 0; z < count; z++) {
          modalCounter = 0;
          currentModal = custom[z][ctxt_modal];
          currentProdsList = custom[z][ctxt_prodlist];
          if (currentModal == true || currentModal == "true") {
            for (y = 0; y < custom[z][ctxt_prodlist].length; y++) {
              //  $.each(d, function(ind, val) {
              if (d[custom[z][ctxt_prodlist][y]] && d[custom[z][ctxt_prodlist][y]]["dispo"] != false) {
                modalCounter++;
              }
              //});
            }
            if (modalCounter < 1) {
              tracker.error({
                "tl": "mandatoryNotFound"
              });
              return removers;
              return false;
            }
          }
        }

        tracker.context._value.tz = "vignette";

        var abkCreateFormat = helper_methods.createFormat(container, tracker, products, d, 100);
        if ($('.resultats-recherche').length != 0) {
          container.closest(".sto-" + placeholder + "-bundleboost").attr("data-page", "search");
        }

        if (container.find('div.sponsorise')) {
          container.find('div.sponsorise').removeAttr('onview-beacon').removeClass('hl-product').removeClass('sponsorise');
        }


        style.use();

        if ($("#crossZone-0").length > 0) {
          var tpl_type;
          tpl_type = "lines";
          var lineSelector, indexSelector;
          var nbProducts = $(".vignette-box").length,
            nbRow = $("#central-container .row").length,
            target;

          if (nbProducts >= 4) {
            lineSelector = position.lines[btfpos].line;
            indexSelector = position.lines[btfpos].index;
            target = $($("#itemsList .row")[lineSelector - 1]).find(".vignette-box").eq(indexSelector - 1);
            $(container).insertBefore(target);
            $(".sto-" + placeholder + " -w-butterfly").attr('data-grid', 'lines');
          } else {
            lineSelector = false;
          }

          if (lineSelector) {
            for (lineSelector; lineSelector <= nbRow; lineSelector++) {
              if ($('#central-container .row' + lineSelector + ' .vignette-box').length == 6) {
                $($('#central-container .row' + lineSelector + ' .vignette-box')[4]).insertBefore($($('#central-container .row' + (lineSelector + 1) + ' .vignette-box')[0]));
                $($('#central-container .row' + lineSelector + ' .vignette-box')[4]).insertBefore($($('#central-container .row' + (lineSelector + 1) + ' .vignette-box')[1]))
              } else {
                var e = 0;
                for (var i = 0; i < 1; i++) {
                  if ($('#central-container .row' + (lineSelector + 1) + ' .vignette-box')[0] === undefined) {
                    $($($('#central-container .row' + (lineSelector + 1))[0])).prepend($('#central-container .row' + lineSelector + ' .vignette-box')[2]);
                  } else {
                    $($('#central-container .row' + lineSelector + ' .vignette-box')[4]).insertBefore($($('#central-container .row' + (lineSelector + 1) + ' .vignette-box')[e]));
                    e++;
                  }
                }
              }
            }
          }
        } else {
          var sto_global_grid_pos = position.grid[btfpos];
          tpl_type = "grid";
          var e = parseInt(sto_global_grid_pos - 1);
          target = $($("#itemsList .vignette-box")[e]);
          $(container).insertBefore(target);

        }

        for (var i = 0; i <= count; i++) {
          browseObj(i, d, tracker);
        } //contains the quantity of products to display

        $('.sto-' + placeholder + '-bundleboost').attr('data-count', $('.sto-' + placeholder + '-modalbox-products>*').length);



        for (var i = 0; i < $('.sto-' + placeholder + '-modalbox-products>*').length; i++) {

          if ($('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length > 1) {
            var optID = $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id');
            var optQTY = $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length;
            options[optID] = {
              "quantity": optQTY
            };
            for (var j = 0; j < $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length - 1; j++) {
              /*$('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').eq(j).attr('sto-prod', 'repeated');*/

              $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').eq(j).remove();
            }

            $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').eq($('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length).append('<div class="sto-repeated-prod">X' + optQTY + '</div>');

            if ($('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').find('.sto-product-repeated').length < 1) {
              $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').eq($('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length - 1).append('<div class="sto-product-repeated">x' + optQTY + '</div>');
            }
          }
        }

        $(window).on('scroll', function() {
          //var pageProdsQty = setInterval(function(){
          if (($('#central-container .vignette').length - $('.sto-' + placeholder + '-bundleboost .vignette').length) > 20) {
            for (var i = 0; i < $('#central-container .row').length; i++) {
              if ($('#central-container .row').eq(i).children('.vignette-box').length > 4) {
                if ($('#central-container .row').eq(i + 1)) {
                  $('#central-container .row').eq(i + 1).prepend($('#central-container .row').eq(i).children('.vignette-box').eq(4));
                }
              }
            }
          }
          //}, 300);
        });




        $('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text($('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text().replace(" + ", ""));

        $('.sto-' + placeholder + '-bundleboost>.vignette .vignette-content .tetiere, .sto-' + placeholder + '-modalbox-header span').text(settings.tetiere_text);

        $('.sto-' + placeholder + '-bundleboost>.vignette .add-to-basket-area>a, .sto-' + placeholder + '-modalbox-cta>span').text(settings.abk_txt);

        if (settings.promo_zone == true) {
          $('.sto-' + placeholder + '-bundleboost>.vignette .infos-prix').before('<div class="tooltip-promo hover"><p class="operation-produit operation-produit-promo"><!--3€ de reduction sur le kit--></p></div>');

          $('.sto-' + placeholder + '-bundleboost>.vignette .operation-produit.operation-produit-promo').text(settings.promo_text.substring(0, 30));
        }

        if ($('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text().length > 70) {
          var cutName = $('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text().substring(0, 70);
          $('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text(cutName + '...');
        }

        $('body').on('click', '.sto-' + placeholder + '-bundleboost>.vignette>.vignette-content>a', function(e) {
          e.preventDefault();
          tracker.open({
            "tl": "clickOnTile"
          });
          $('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox').show(0);
          $('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox').animate({
            "opacity": 1
          }, 300);
          tracker.context._value.tz = "modalBox";
        });

        $('body').on('click', '.sto-' + placeholder + '-modalbox-close', function(e) {
          e.preventDefault();
          tracker.close({
            "tl": "btnClose"
          })
          $('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox').animate({
            "opacity": 0
          }, 300, function() {
            $('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox').hide(0);
          });
          tracker.context._value.tz = "vignette";
        });

        $('body').on('click', '.sto-' + placeholder + '-w-modalbox', function(e) {
          e.preventDefault();
          tracker.close({
            "tl": "outSideBox"
          })
          $('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox').animate({
            "opacity": 0
          }, 300, function() {
            $('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox').hide(0);
          });
          tracker.context._value.tz = "vignette";
        });

        $('body').on('click', '.fiche-produit .close-btn', function(e) {
          e.preventDefault();
          tracker.close({
            "tl": "closeFP"
          })
          $('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox').animate({
            "opacity": 0
          }, 300, function() {
            $('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox').hide(0);
          });
          tracker.context._value.tz = "vignette";
        });



        $('body').one('click', '.sto-' + placeholder + '-bundleboost>.vignette .add-to-basket-area.sto-' + placeholder + '-button-on, .sto-' + placeholder + '-modalbox-cta.sto-global-abk.sto-' + placeholder + '-button-on', function(e) {
          $('body').append('<iframe id="sto-cookie-' + placeholder + '-bbs" src="//dynamic-content.auchandrive.fr/ck?c=bbs-adr-' + settings.iid + '&a=s&v=1"></iframe>');
          setTimeout(function() {
            $('#sto-cookie-' + placeholder + '-bbs').remove();
          }, 3000)
          e.preventDefault();
          bundleboost_add = false;
          /*var options = {
            "356845": {
              "quantity": 2
            }
          };*/

          abkCreateFormat.addAllProducts(options);
        });

        //dynamic-content.auchandrive.fr/ck?c=lotusbaby&a=s&v=1

        $('body').on('click', '.sto-' + placeholder + '-bundleboost>.vignette .add-to-basket-area.sto-' + placeholder + '-button-off, .sto-' + placeholder + '-modalbox-cta.sto-global-abk.sto-' + placeholder + '-button-off', function(e) {
          e.preventDefault();
          e.stopPropagation();
        });

        var maximized_products, matched_ids, highest_qty, highest_ids, total_products,
          int = window.setInterval(function() {
            maximized_products = [];
            highest_ids = [];
            total_products = 0;
            highest_qty = -1
            $('#sto-format').find(".sto-" + placeholder + "-modalbox-products .add-to-basket-area").each(function() {
              var current_qty = parseInt(jQuery(this).find('input[name="quantityArticleToSubmit"]').val()),
                max_reached = jQuery(this).find(".qte-max").length,
                product_id = jQuery(this).closest(".sto-product").data("id");
              if (current_qty >= highest_qty) {
                current_qty == highest_qty ? highest_ids.push(product_id) : highest_ids = [product_id];
                highest_qty = current_qty;
              };
              if (max_reached) {
                maximized_products.push(product_id);
              }
              total_products++
            });
            matched_ids = highest_ids.filter(function(highest_product_id) {
              return maximized_products.indexOf(highest_product_id) == -1
            });

            if (maximized_products.length || bundleboost_add == false) {
              //if (maximized_products.length && (matched_ids.length || maximized_products.length === total_products || highest_ids.length < maximized_products.length)) {
              $('.sto-' + placeholder + '-modalbox-cta,#sto-format.sto-' + placeholder + '-bundleboost>.vignette .add-to-basket-area').addClass('sto-' + placeholder + '-button-off');
              $('.sto-' + placeholder + '-modalbox-cta,#sto-format.sto-' + placeholder + '-bundleboost>.vignette .add-to-basket-area').removeClass('sto-' + placeholder + '-button-on');
            } else {
              $('.sto-' + placeholder + '-modalbox-cta,#sto-format.sto-' + placeholder + '-bundleboost>.vignette .add-to-basket-area').addClass('sto-' + placeholder + '-button-on');
              $('.sto-' + placeholder + '-modalbox-cta,#sto-format.sto-' + placeholder + '-bundleboost>.vignette .add-to-basket-area').removeClass('sto-' + placeholder + '-button-off');
            }

            /*if (maximized_products.length && bundleboost_add == true) {
              $('.sto-' + placeholder + '-modalbox-cta').append('<div class="sto-' + placeholder + '-message"><span>Un des produits promotionels a déjà atteint la quantité maximum</span></div>');
            }*/

          }, 100);

        /*if(settings.iid){
          console.log(settings.iid);
        }*/


      }).then(null, console.log.bind(console));
    });

    return removers;
  });
}

function navigateArray() {

}

function setTetiereType() {
  $('.sto-' + placeholder + '-w-butterfly .vignette-content').each(function() {
    var self = $(this);
    if (self.hasClass('promo')) {
      self.find('.tetiere').attr('data-tetiere', 'promo')
    } else if (self.hasClass('fidelite')) {
      self.find('.tetiere').attr('data-tetiere', 'fidelite')
    } else {
      self.find('.tetiere').css({
        "opacity": "0"
      })
    }
  });
}

function verifAvailability(a, d) {
  var l = a[ctxt_prodlist].length;
  for (var i = 0; i <= (l - 1); i++) {
    var value = a[ctxt_prodlist][i];
    if (d[value] !== undefined && d[value].dispo !== false) {
      sto_global_products.push(value);
      sto_global_nbproducts += 1;
      return value;
    }
  }
  return false;
}

var finalPrice = 0;

function addButton(cug, title, index) {
  var currentProd = $('.sto-' + placeholder + '-bundleboost .sto-product-container>div[data-id="' + cug + '"]').detach();
  $('.sto-' + placeholder + '-modalbox-products').append(currentProd);
  var currentTitle = $('.sto-' + placeholder + '-modalbox-products>div[data-id="' + cug + '"] .libelle-produit').eq(0).text();
  var currentImg = $('.sto-' + placeholder + '-modalbox-products>div[data-id="' + cug + '"] .visuel-produit img').eq(0).clone();
  if (settings.lable_text == "") {
    $('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text($('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text() + ' + ' + currentTitle);
  } else {
    $('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text(settings.lable_text.substring(0, 70));
  }
  if (settings.tile_main_image == "") {
    $('.sto-' + placeholder + '-bundleboost>.vignette .visuel-produit').append(currentImg);
  }


  var currentPrice = parseFloat($('.sto-' + placeholder + '-bundleboost div[data-id="' + cug + '"] .prix-produit').text().replace(',', '.'));
  finalPrice = finalPrice + currentPrice;
  $('.sto-' + placeholder + '-bundleboost>.vignette .prix-produit').text(finalPrice.toString().split('.')[0]);
  $('.sto-' + placeholder + '-bundleboost>.vignette .prix-produit').append('<span>,' + finalPrice.toFixed(2).toString().split('.')[1] + '</span><span class="prix-symbole-euro">€</span>');
}

function browseObj(index, d, tracker) {
  var currentbObj = custom[index];
  if (currentbObj) {
    var title = currentbObj[ctxt_title],
      modal = currentbObj[ctxt_modal],
      available = verifAvailability(currentbObj, d);
    modal = modal === "true" || modal === true ? true : false;
    if (available != false) {
      if (modal) {
        sto_global_modal = true;
      }
      addButton(available, title, index, tracker);
    }
  }

}

function firstDisplay() {
  //$('.sto-' + placeholder + '-w-butterfly .sto-product-container .vignette:first-child').show();
  $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).addClass('sto-' + placeholder + '-selButton');
  if (settings.custom_images === true) {
    $('.sto-' + placeholder + '-w-butterfly').attr('data-index', $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).attr('data-index'));
  }
}

function selectProduct(product, index) {
  if (settings.custom_images === true) {
    $('.sto-' + placeholder + '-w-butterfly').attr('data-index', index);
  }
  $('.sto-' + placeholder + '-w-butterfly .sto-product-container .vignette').hide();
  $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button').removeClass('sto-' + placeholder + '-selButton');
  $('.sto-' + placeholder + '-w-butterfly .sto-product-container .vignette[data-id="' + product + '"]').show();
  $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button#' + product).addClass('sto-' + placeholder + '-selButton');
}
