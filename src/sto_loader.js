var settings = require("./settings.json");
settings.tetiere_txt_color = settings.tetiere_txt_color === "" ? "transparent" : settings.tetiere_txt_color;
settings.tetiere_bg_color = settings.tetiere_bg_color === "" ? "transparent" : settings.tetiere_bg_color;
settings.promo_txt_color = settings.promo_txt_color === "" ? "transparent" : settings.promo_txt_color;
settings.promo_bg_color = settings.promo_bg_color === "" ? "transparent" : settings.promo_bg_color;
settings.tile_main_image = settings.tile_main_image === "" ? "none" : "url(./../../img/" + settings.tile_main_image + ")";
module.exports = function(source) {
  this.cacheable();
  var test = source
    .replace(/__PLACEHOLDER__/g, `${settings.format}_${settings.name}_${settings.creaid}`)
    .replace(/__FORMAT__/g, settings.format)
    .replace(/__TILE_IMG__/g, settings.tile_main_image)
    .replace(/__TETIERE_TXT_COLOR__/g, settings.tetiere_txt_color)
    .replace(/__TETIERE_BG_COLOR__/g, settings.tetiere_bg_color)
    .replace(/__PROMO_TXT_COLOR__/g, settings.promo_txt_color)
    .replace(/__PROMO_BG_COLOR__/g, settings.promo_bg_color);
  return test;
};
