"use strict";

const ctxt_pos = 0,
  ctxt_title = 1,
  ctxt_modal = 2,
  ctxt_prodlist = 3;

var sto = window.__sto,
  settings = require("./../../settings.json"),
  $ = window.jQuery,
  html = require("./main.html"),
  placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
  formatID = `${placeholder}_${Math.random().toString(36).substring(7)}`,
  style = require("./main.css"),
  container = $(html),
  format = settings.format,
  custom = settings.custom,
  products = [],
  btfpos = settings.btfpos,
  container = $(html),
  helper_methods = sto.utils.retailerMethod,
  url_crawling = settings.crawl,
  sto_global_nbproducts = 0,
  sto_global_products = [],
  sto_global_modal,
  promise,
  pdf;

var fontRoboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
$('head').first().append(fontRoboto);

custom.sort(function(a, b) {
  return a[ctxt_pos] > b[ctxt_pos]
});
// Get all product ids from custom product organization
custom.forEach(function(e) {
  e[ctxt_prodlist].forEach(function(p) {
    products.push(p);
  })
});


module.exports = {
  init: _init_(),
  styleGutter: style
}

var local_helpers = {
  oldWidth: -1,
  isLoaded: false,
  tracker: null,
  // Find position of product in DOM
  getProductPos: function(productId) {
    var p = -1;
    $("#productListContainerWrapper .cd-Product").each(function(i, v) {
      if ($(this).find(".cd-ProductInfosActions").attr("data-id") === productId) {
        p = i;
      }
    });
    return p;
  },
  download: function(tracker) {
    var textButton = "<div class='sto-text-download'><span>" + settings.text_recette + "</span></div>";
    formatDisplay.find('.sto-' + placeholder + '-download').append(textButton);
    this.setDownloadClicks(tracker);
  },
  setDownloadClicks: function(tracker) {
    var that = this;
    var element;
    if (settings.cta_area === "vignette") {
      $('.sto-' + placeholder + '-container').css("cursor", "pointer");
      $('body').on("click", '.sto-' + placeholder + '-container', function(event) {
        chooseEvent(tracker);
        event.stopPropagation();
      });
    } else if (settings.cta_area !== "vignette") {
      formatDisplay.on("click", '.sto-' + placeholder + '-download', function(event) {
        chooseEvent(tracker);
        event.stopPropagation();
      });
    }
  },
  // Organize tiles
  placeTiles: function(place, isHere) {
    var containerWidth = $('#productListContainerWrapper').outerWidth(),
      domPos = $('#productListContainerWrapper .js-crossBlock').index(formatDisplay) - 1,
      productWidth = $('#productListContainerWrapper .cd-Product').width(),
      nbProducts = Math.floor(containerWidth / productWidth),
      productPos = place % nbProducts;
    if (this.oldWidth !== containerWidth || window.__sto_variables[settings.format]['old_format'] != window.__sto_variables[settings.format]['format'] || place !== domPos || (productPos === (nbProducts - 1))) {
      var productBlock = $('#productListContainerWrapper .cd-Product:eq(' + place + ')');

      this.oldWidth = containerWidth;

      window.__sto_variables[settings.format]['old_format'] = window.__sto_variables[settings.format]['format'];

      // If we are at the end of the line
      if (productPos + 1 >= nbProducts) {
        // We move blocks
        var productNext = $('#productListContainerWrapper .cd-Product:eq(' + (place + 1 + (isHere ? 1 : 0)) + ')');

        if (productNext.length > 0) {
          productBlock.insertAfter(productNext);
        } else {
          productBlock.prev().insertAfter(productBlock);
        }
      }

      if (productBlock.length > 0 && !isHere) {
        productBlock.after(formatDisplay);
        this.isLoaded = true;
        this.tracker.display();
        var ctxt = $("#" + formatID).find(".cd-ProductData");
        if (window.tc_events_3) {
          try {
            var tc_event_sto = {
              "storetail": "Impression", // ou "Impression" pour les impressions
              "storetail_adtype": this.tracker.value("re"),
              "storetail_creative": this.tracker.value("tc"),
              "storetail_EAN": ctxt.data(settings.position_format),
              "storetail_productName": ctxt.data(""),
              "storetail_price": ctxt.data("")
            };
            window.tc_events_3($("#" + formatID), "storetail", tc_event_sto);
            console.log(tc_event_sto);
          } catch (e) {
            console.log("none");
          }
        }
        // this.download();
      } else {
        formatDisplay.insertAfter(productBlock);
        // this.download();
      }
    }
  }
};

function _init_() {


  sto.load(format, function(tracker) {

    helper_methods.crawl(url_crawling, true).promise.then(function(d) {

      var modalCounter = 0,
        currentModal,
        currentProdsList,
        z,
        y,
        count = custom.length;

      for (z = 0; z < count; z++) {
        currentModal = custom[z][ctxt_modal];
        currentProdsList = custom[z][ctxt_prodlist];
        if (currentModal == true || currentModal == "true") {
          for (y = 0; y < custom[z][ctxt_prodlist].length; y++) {
            $.each(d, function(ind, val) {
              if (ind == custom[z][ctxt_prodlist][y]) {
                modalCounter++;
              }
            });
          }
          if (modalCounter < 1) {
            tracker.error({
              "tl": "mandatoryNotFound"
            });
            return false;
          }
        }
      }

      var abkCreateFormat = helper_methods.createFormat(container, tracker, products, d, 100);

      style.use();

      $('#productListContainerWrapper>.cd-Product').eq(1).before(container);


      for (var i = 0; i <= count; i++) {
        browseObj(i, d, tracker);
      } //contains the quantity of products to display

      $('.sto-' + placeholder + '-bundleboost').attr('data-count', $('.sto-' + placeholder + '-modalbox-temporary-container>*').length);

      $('.sto-' + placeholder + '-modalbox-temporary-container>*').appendTo('.sto-' + placeholder + '-modalbox-products');

      for (var i = 0; i < $('.sto-' + placeholder + '-modalbox-products>*').length; i++) {

        if ($('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length > 1) {
          for (var j = 0; j < $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length - 1; j++) {
            $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').eq(j).attr('sto-prod', 'repeated');
          }

          $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').eq($('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length).append('<div class="sto-repeated-prod">X' + $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length + '</div>');

          if ($('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').find('.sto-product-repeated').length < 1) {
            $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').eq($('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length - 1).append('<div class="sto-product-repeated">x' + $('.sto-' + placeholder + '-modalbox-products>*[data-id="' + $('.sto-' + placeholder + '-modalbox-products>*').eq(i).attr('data-id') + '"]').length + '</div>');
          }

        }
      }




      $('.sto-' + placeholder + '-bundleboost>.cd-ProductContainer h4>a').text($('.sto-' + placeholder + '-bundleboost>.cd-ProductContainer h4>a').text().replace(" + ", ""));

      $('.sto-' + placeholder + '-bundleboost>.cd-ProductContainer .cd-ProductBannerTitle.js-displayBannerLayer, .sto-' + placeholder + '-modalbox-header span').text(settings.tetiere_text);

      /*$('.sto-' + placeholder + '-bundleboost>.vignette .add-to-basket-area>a, .sto-' + placeholder + '-modalbox-cta>span').text(settings.abk_txt);

      if (settings.promo_zone == true) {
        $('.sto-' + placeholder + '-bundleboost>.vignette .infos-prix').before('<div class="tooltip-promo hover"><p class="operation-produit operation-produit-promo"><!--3€ de reduction sur le kit--></p></div>');

        $('.sto-' + placeholder + '-bundleboost>.vignette .operation-produit.operation-produit-promo').text(settings.promo_text.substring(0, 25));
      }*/

      /*if ($('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text().length > 70) {
        var cutName = $('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text().substring(0, 70);
        $('.sto-' + placeholder + '-bundleboost>.vignette .libelle-produit').text(cutName + '...');
      }*/

      $('body').on('click', '.sto-' + placeholder + '-bundleboost>.cd-ProductContainer a', function(e) {
        e.preventDefault();
        tracker.open({
          "tl": "clickOnTile"
        });

        $('.r-Grid.cd-HeaderGrid').addClass('sto-cover');
        /*$('body').append('<div class="sto-' + placeholder + '-modalbox"><div class="sto-' + placeholder + '-modalbox-header"><span>DANS VOTRE KIT VACANCES</span></div><div class="sto-' + placeholder + '-modalbox-close"></div> <div class="sto-' + placeholder + '-modalbox-products"></div> <div class="sto-' + placeholder + '-modalbox-cta sto-global-abk"><span></span></div> </div> <div class="sto-' + placeholder + '-w-modalbox">');
        $('.sto-' + placeholder + '-modalbox-temporary-container>*').appendTo('.sto-' + placeholder + '-modalbox-products');*/
        $('.sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-modalbox').show();
        $('.sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-modalbox').animate({
          "opacity": 1
        }, 300);
      });

      $('body').on('click', '.sto-' + placeholder + '-modalbox-close', function(e) {
        e.preventDefault();
        tracker.close({
          "tl": "btnClose"
        })
        $(' .sto-' + placeholder + '-modalbox').animate({
          "opacity": 0
        }, 300, function() {
          //$('.sto-' + placeholder + '-modalbox-products>*').appendTo('.sto-' + placeholder + '-modalbox-temporary-container');
          $('.sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-modalbox').hide();
          $('.r-Grid.cd-HeaderGrid').removeClass('sto-cover');
        });
      });

      $('body').on('click', '.sto-' + placeholder + '-w-modalbox', function(e) {
        e.preventDefault();
        tracker.close({
          "tl": "outSideBox"
        })
        $(' .sto-' + placeholder + '-modalbox').animate({
          "opacity": 0
        }, 300, function() {
          //$('.sto-' + placeholder + '-modalbox-products>*').appendTo('.sto-' + placeholder + '-modalbox-temporary-container');
          $('.sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-modalbox').hide();
          $('.r-Grid.cd-HeaderGrid').removeClass('sto-cover');
        });
      });

      $('body').on('click', '.fiche-produit .close-btn', function(e) {
        e.stopPropagation();
        e.preventDefault();
        tracker.close({
          "tl": "closeFP"
        })
        $('.sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-w-modalbox, .sto-' + placeholder + '-bundleboost .sto-' + placeholder + '-modalbox').animate({
          "opacity": 0
        }, 300, function() {
          $('.sto-' + placeholder + '-modalbox').hide(0);
          $('.r-Grid.cd-HeaderGrid').removeClass('sto-cover');
        });
      });



      $('body').on('click', '.sto-' + placeholder + '-bundleboost>.cd-ProductContainer .sto-global-abk, .sto-' + placeholder + '-modalbox-cta.sto-global-abk', function(e) {
        e.preventDefault();
        abkCreateFormat.addAllProducts();
      });

    });

    var removers = {};
    removers[format] = function() {
      style.unuse();
      container.remove();
    };
    return removers;
  });
}

function navigateArray() {

}

function setTetiereType() {
  $('.sto-' + placeholder + '-w-butterfly .vignette-content').each(function() {
    var self = $(this);
    if (self.hasClass('promo')) {
      self.find('.tetiere').attr('data-tetiere', 'promo')
    } else if (self.hasClass('fidelite')) {
      self.find('.tetiere').attr('data-tetiere', 'fidelite')
    } else {
      self.find('.tetiere').css({
        "opacity": "0"
      })
    }
  });
}

function verifAvailability(a, d) {
  var l = a[ctxt_prodlist].length;
  for (var i = 0; i <= (l - 1); i++) {
    var value = a[ctxt_prodlist][i];
    if (d[value] !== undefined && d[value].dispo !== false) {
      sto_global_products.push(value);
      sto_global_nbproducts += 1;
      return value;
    }
  }
  return false;
}

var finalPrice = 0;

function addButton(cug, title, index) {

  var currentProd = $('.sto-' + placeholder + '-bundleboost .sto-product-container>*[data-id="' + cug + '"]').detach();
  $('.sto-' + placeholder + '-modalbox-temporary-container').append(currentProd);
  var currentTitle = $('.sto-' + placeholder + '-modalbox-temporary-container>*[data-id="' + cug + '"] h4').attr('title');
  var currentImg = $('.sto-' + placeholder + '-modalbox-temporary-container>*[data-id="' + cug + '"] .cd-ProductVisual>a>img').eq(0).clone();
  if (settings.lable_text == "") {
    $('.sto-' + placeholder + '-bundleboost>.cd-ProductContainer h4>a').text($('.sto-' + placeholder + '-bundleboost>.cd-ProductContainer h4>a').text() + ' + ' + currentTitle);
  } else {
    $('.sto-' + placeholder + '-bundleboost>.cd-ProductContainer h4>a').text(settings.lable_text.substring(0, 65));
  }
  $('.sto-' + placeholder + '-bundleboost>.cd-ProductContainer .cd-ProductVisual>a').append(currentImg);
  var currentPrice = parseFloat($('.sto-' + placeholder + '-bundleboost div[data-id="' + cug + '"] .cd-ProductPriceUnit').text().replace(',', '.'));
  finalPrice = finalPrice + currentPrice;
  $('.sto-' + placeholder + '-bundleboost>.cd-ProductContainer .cd-ProductPriceUnit').text(finalPrice.toFixed(2).toString().split('.')[1] + '€');
  $('.sto-' + placeholder + '-bundleboost>.cd-ProductContainer .cd-ProductPriceUnit').prepend('<span class="cd-ProductPriceUnitInteger">' + finalPrice.toString().split('.')[0] + ',</span>');
}

function browseObj(index, d, tracker) {
  var currentbObj = custom[index];
  if (currentbObj) {
    var title = currentbObj[ctxt_title],
      modal = currentbObj[ctxt_modal],
      available = verifAvailability(currentbObj, d);
    modal = modal === "true" || modal === true ? true : false;
    if (available != false) {
      if (modal) {
        sto_global_modal = true;
      }
      addButton(available, title, index, tracker);
    }
  }

}

function firstDisplay() {
  $('.sto-' + placeholder + '-w-butterfly .sto-product-container .vignette:first-child').show();
  $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).addClass('sto-' + placeholder + '-selButton');
  if (settings.custom_images === true) {
    $('.sto-' + placeholder + '-w-butterfly').attr('data-index', $($('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button')[0]).attr('data-index'));
  }
}

function selectProduct(product, index) {
  if (settings.custom_images === true) {
    $('.sto-' + placeholder + '-w-butterfly').attr('data-index', index);
  }
  $('.sto-' + placeholder + '-w-butterfly .sto-product-container .vignette').hide();
  $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button').removeClass('sto-' + placeholder + '-selButton');
  $('.sto-' + placeholder + '-w-butterfly .sto-product-container .vignette[data-id="' + product + '"]').show();
  $('.sto-' + placeholder + '-w-butterfly .sto-' + placeholder + '-w-buttons button#' + product).addClass('sto-' + placeholder + '-selButton');
}
